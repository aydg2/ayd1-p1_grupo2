function factorial(param){
    return   param === 0 ? 1 : param * factorial(param - 1)
}